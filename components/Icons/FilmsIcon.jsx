import React from "react";
import { StyleSheet, Image } from 'react-native';

function FilmsIcon() {
  return (
    <>
      <Image style={styles.stdIcon} source={require('../../images/films.png')} />
    </>
  );
}
const styles = StyleSheet.create({
  stdIcon:{
      width:40,
      height:40
  }
});

export default FilmsIcon;
