import FilmsIcon from "./FilmsIcon";
import GraphIcon from "./GraphIcon";
import StudyIcon from "./StudyIcon";
import TimeIcon from "./TimeIcon";
import PhotosIcon from './PhotosIcon';

export {FilmsIcon, GraphIcon, StudyIcon, TimeIcon, PhotosIcon};
