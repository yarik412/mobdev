import React from "react";
import { LineChart, XAxis, YAxis } from "react-native-svg-charts";
import { View } from "react-native";
import { G, Line } from "react-native-svg";

class CustomGridExample extends React.PureComponent {
  render() {
    const data = [25, 16, 9, 4, 1, 0, 1, 4, 9, 16, 25];
    const axesSvg = { fontSize: 10, fill: "grey" };
    const verticalContentInset = { top: 15, bottom: 5 };
    const zeroValue = 5;

    const CustomGrid = ({ x, y, data, ticks }) => (
      <G>
        {
          // Horizontal grid
          <Line
            key={1}
            x1={"0%"}
            x2={"100%"}
            y1={y(0)}
            y2={y(0)}
            stroke={"rgba(0,0,0,0.2)"}
          />
        }
        {
          // Vertical grid
          
          <Line
            key={5}
            y1={"0%"}
            y2={"100%"}
            x1={x(zeroValue)}
            x2={x(zeroValue)}
            stroke={"rgba(0,0,0,0.2)"}
          />
        }
      </G>
    );

    return (
      <View style={{ height: 200, width: 200, flexDirection: "row" }}>
        <YAxis
          data={data}
          style={{ marginBottom: 0 }}
          contentInset={verticalContentInset}
          svg={axesSvg}
        />
        <LineChart
          style={{ flex: 1 }}
          data={data}
          svg={{
            stroke: "rgb(134, 65, 244)",
          }}
        >
          <XAxis
            style={{ marginHorizontal: -10, height: 30 }}
            data={data}
            formatLabel={(value, index) => value - zeroValue}
            contentInset={{ left: 10, right: 10 }}
            svg={axesSvg}
          />
          <CustomGrid belowChart={true} />
        </LineChart>
      </View>
    );
  }
}

export default CustomGridExample;
