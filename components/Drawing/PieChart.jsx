import React from 'react'
import { PieChart } from 'react-native-svg-charts'

class PieChartCustom extends React.PureComponent {

    render() {

        const data = [
            {
                key: 1,
                value: 40,
                svg: { fill: 'yellow' },
                arc: { outerRadius: '130%', cornerRadius: 10,  }
            },
            {
                key: 2,
                value: 25,
                svg: { fill: 'red' }
            },
            {
                key: 3,
                value: 35,
                svg: { fill: 'green' }
            },
        ]


        return (
            <PieChart
                style={{ height: 200, width:200 }}
                outerRadius={'70%'}
                innerRadius={10}
                data={data}
            />
        )
    }

}

export default PieChartCustom;