import React, { useState, useEffect } from "react";
import {
  Button,
  Image,
  View,
  ScrollView,
  Platform,
  StyleSheet,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { DynamicCollage, StaticCollage } from "react-native-images-collage";

export default function Photos() {
  const [images, setImage] = useState([]);
  const [images1, setImage1] = useState([]);
  const [matrix, setMatrix] = useState([]);
  const [matrix1, setMatrix1] = useState([]);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
    });

    if (!result.cancelled) {
      if (matrix.length >= 3) {
        (matrix1.length + 1) === 1
          ? setMatrix1([...matrix1, 1], setImage1([...images1, result.uri]))
          : (setMatrix1([...matrix1, 2]),
            setImage1([...images1, result.uri, result.uri]));
      } else {
        (matrix.length + 1) % 2 === 0
          ? setMatrix([...matrix, 1], setImage([...images, result.uri]))
          : (setMatrix([...matrix, 2]),
            setImage([...images, result.uri, result.uri]));
      }
    }
  };

  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 40,
      }}
    >
      <Button title="Pick an image from camera roll" onPress={pickImage} />
      <StaticCollage
        width={400}
        height={300}
        images={images}
        matrix={matrix}
        direction={"row"}
        panningRightPadding={10}
        isEditButtonVisible={true}
        containerStyle={{borderWidth:0, flex:1}}
        imageContainerStyle={{ width: 60, height: 60, borderWidth:3, borderColor:'gray' }}
      />
      {matrix.length >= 3 && (
        <StaticCollage
          width={400}
          height={300}
          images={images1}
          matrix={matrix1}
          direction={"row"}
          panningRightPadding={10}
          isEditButtonVisible={true}
          containerStyle={{borderWidth:0, flex:1}}
          imageContainerStyle={{ width: 60, height: 60, borderWidth:3, borderColor:'gray' }}
        />
      )}
      {/* <ScrollView>
        <View style={styles.album}>
          {images &&
            images.map((image, index) => (
              <Image
                key={index}
                source={{ uri: image }}
                style={(index+1) % 3 === 0 && index !== 9 ? styles.first : styles.second}
              />
            ))}
        </View>
      </ScrollView> */}
    </View>
  );
}

const styles = StyleSheet.create({
  first: {
    width: 200,
    height: 200,
    borderWidth: 3,
    borderColor: "gray",
  },
  second: {
    width: 100,
    height: 100,
    borderWidth: 3,
    borderColor: "gray",
  },
  album: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
  },
});
