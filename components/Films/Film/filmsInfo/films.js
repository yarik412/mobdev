import * as tt0076759 from './tt0076759.json';
import * as tt0080684 from './tt0080684.json';
import * as tt0086190 from './tt0086190.json';
import * as tt0120915 from './tt0120915.json';
import * as tt0121765 from './tt0121765.json';
import * as tt0121766 from './tt0121766.json';
import * as tt2488496 from './tt2488496.json';
import * as tt2527336 from './tt2527336.json';
import * as tt3748528 from './tt3748528.json';
import * as tt0796366 from './tt0796366.json';

export {
    tt0076759,
    tt0080684,
    tt0086190,
    tt0120915,
    tt0121765,
    tt0121766,
    tt2488496,
    tt2527336,
    tt3748528,
    tt0796366,
}
