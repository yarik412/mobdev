import React, { PureComponent } from "react";
import { View, Button, Text, Image, StyleSheet } from "react-native";
import * as filmsInfo from "./filmsInfo/films";

class Film extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  render() {
    if (this.state.hasError) {
      return (
        <View>
          <Text>Something went wrong.</Text>
        </View>
      );
    }
    return (
      <View>
        <Image style={styles.imageSize} source={this.props.poster} />
        <Text>Title: {filmsInfo[this.props.id].Title}</Text>
        <Text>Year: {filmsInfo[this.props.id].Year}</Text>
        <Text>Rated: {filmsInfo[this.props.id].Rated}</Text>
        <Text>Released: {filmsInfo[this.props.id].Released}</Text>
        <Text>Runtime: {filmsInfo[this.props.id].Runtime}</Text>
        <Text>Genre: {filmsInfo[this.props.id].Genre}</Text>
        <Text>Director: {filmsInfo[this.props.id].Director}</Text>
        <Text>Writer: {filmsInfo[this.props.id].Writer}</Text>
        <Text>Actors: {filmsInfo[this.props.id].Actors}</Text>
        <Text>Plot: {filmsInfo[this.props.id].Plot}</Text>
        <Text>Language: {filmsInfo[this.props.id].Language}</Text>
        <Text>Country: {filmsInfo[this.props.id].Country}</Text>
        <Text>Awards: {filmsInfo[this.props.id].Awards}</Text>
        <Text>imdbRating: {filmsInfo[this.props.id].imdbRating}</Text>
        <Text>imdbVotes: {filmsInfo[this.props.id].imdbVotes}</Text>
        <Text>Type: {filmsInfo[this.props.id].Type}</Text>
        <Text>Production: {filmsInfo[this.props.id].Production}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageSize: {
    width: 100,
    height: 100,
    margin: 20,
    borderWidth: 1,
    borderColor: "gray",
  },
});

export default Film;
