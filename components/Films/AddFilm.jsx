import React, { useState } from "react";
import {
  SafeAreaView,
  Text,
  TextInput,
  Button,
  StyleSheet,
  View,
} from "react-native";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import * as FileSystem from "expo-file-system";
import * as Permissions from "expo-permissions";

const yearValidation = (film) =>
  film.Year > 1500 && film.Year < 2022 ? true : false;
const titleValidation = (film) => (film.Title.trim() ? true : false);
const typeValidation = (film) => (film.Type.trim() ? true : false);

function AddFilm(props) {
  const [Title, setTitle] = useState("");
  const [Year, setYear] = useState("");
  const [Type, setType] = useState("");

  const addFilmToJson = async (film) => {
    if (!titleValidation(film)) {
      alert("Please set the Title");
    } else if (!yearValidation(film)) {
      alert("Please set the correct year");
    } else if (!typeValidation(film)) {
      alert("Please set the type");
    } else {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status === "granted") {
        try {
          let fileUri = FileSystem.documentDirectory + "MoviesList.txt";
          let txt = await FileSystem.readAsStringAsync(fileUri);
          txt = JSON.parse(txt);
          await FileSystem.writeAsStringAsync(
            fileUri,
            JSON.stringify({
              Search: [
                ...txt.Search,
                {
                  Title: film.Title,
                  Year: film.Year,
                  Type: film.Type,
                  id: uuidv4(),
                },
              ],
            }),
            { encoding: FileSystem.EncodingType.UTF8 }
          );
          props.showDefaultPage();
        } catch (e) {
          alert(e);
        }
      }
    }
  };

  const readFile = async () => {
    try {
      let fileUri = FileSystem.documentDirectory + "MoviesList.txt";
      const txt = await FileSystem.readAsStringAsync(fileUri);
      alert(txt);
    } catch (e) {
      alert(e);
    }
  };

  return (
    <SafeAreaView>
      <Text>Title:</Text>
      <TextInput onChangeText={setTitle} value={Title} placeholder="Title" />
      <Text>Year:</Text>
      <TextInput
        onChangeText={setYear}
        value={Year}
        placeholder="Year"
        keyboardType="numeric"
      />
      <Text>Type:</Text>
      <TextInput onChangeText={setType} value={Type} placeholder="Type" />
      <View style={styles.margButton}>
        <Button
          title="Save"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
          onPress={() => addFilmToJson({ Title, Year, Type })}
        />
      </View>
      <View style={styles.margButton}>
        <Button
          title="Hide"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
          onPress={() => props.showDefaultPage()}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  margButton: {
    paddingTop: 20,
  },
});

export default AddFilm;
