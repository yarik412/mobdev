import React, { PureComponent } from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableHighlight,
  Button,
} from "react-native";
import * as FileSystem from "expo-file-system";
import * as Permissions from "expo-permissions";
import filmsMock from "./MoviesList.json";
import paths from "./postersPath";
import Film from "./Film/Film";
import AddFilm from "./AddFilm";

class Films extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      showFilm: false,
      showAddFilm: false,
      filmId: "",
      posterPath: "",
      films: [],
    };
    this.showDefaultPage = this.showDefaultPage.bind(this);
  }

  showFilm(id, posterPath) {
    if (!id) {
      alert("This film haven't details");
    } else if (id !== "noid") {
      this.setState({
        showFilm: true,
        filmId: id,
        posterPath,
      });
    } else {
      alert("It's only for test");
    }
  }
  showDefaultPage() {
    this.setState({
      showFilm: false,
      showAddFilm: false,
      filmId: "",
      posterPath: "",
    });
  }
  addFilm() {
    this.setState({
      showAddFilm: true,
    });
  }
  async deleteFilm(id) {
    this.setState({
      films:this.state.films.filter((film)=>{
        if(film.id){
          return film.id !== id
        }else{
          return film
        }
      })
    });
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === "granted") {
      try {
        let fileUri = FileSystem.documentDirectory + "MoviesList.txt";
        let txt = await FileSystem.readAsStringAsync(fileUri);
        txt = JSON.parse(txt);
        await FileSystem.writeAsStringAsync(
          fileUri,
          JSON.stringify({
            Search: txt.Search.filter((film) => film.id !== id),
          }),
          { encoding: FileSystem.EncodingType.UTF8 }
        );
      } catch (e) {
        alert(e);
      }
    }
  }

  componentDidMount() {
    (async () => {
      try {
        let fileUri = FileSystem.documentDirectory + "MoviesList.txt";
        const txt = await FileSystem.readAsStringAsync(fileUri);
        txt ? txt = txt :(txt = JSON.stringify({ Search: [] }));
        this.setState({
          films: [...filmsMock.Search, ...JSON.parse(txt).Search],
        });
      } catch (e) {
        alert(e);
      }
    })();
  }

  render() {
    if (this.state.hasError) {
      return (
        <View>
          <Text>Something went wrong.</Text>
        </View>
      );
    }
    return (
      <ScrollView style={[styles.margTop, styles.mainSize]}>
        {(() => {
          if (this.state.showAddFilm) {
            return <AddFilm showDefaultPage={this.showDefaultPage}/>;
          } else if (this.state.showFilm) {
            return (
              <>
                <Button
                  title="Hide"
                  color="#841584"
                  accessibilityLabel="Learn more about this purple button"
                  onPress={() => this.showDefaultPage()}
                />
                <Film
                  id={this.state.filmId}
                  poster={this.state.posterPath}
                  func={this.hideFilm}
                />
              </>
            );
          } else {
            return (
              <>
                <Button
                  title="Add Film"
                  color="#841584"
                  accessibilityLabel="Learn more about this purple button"
                  onPress={() => this.addFilm()}
                />
                {this.state.films.map((film, index) => {
                  const id = film.id ? film.id : false;
                  const imdbID = film.imdbID ? film.imdbID : "";
                  const imagePath = paths[index] ? paths[index].image : "";
                  return (
                    <TouchableHighlight
                      onPress={() => this.showFilm(imdbID, imagePath)}
                      key={index}
                    >
                      <View style={styles.film}>
                        {paths[index] ? (
                          <Image
                            style={styles.imageSize}
                            source={paths[index].image}
                          />
                        ) : (
                          <View style={styles.imageSize} />
                        )}
                        <View style={styles.infoBlock}>
                          <Text style={styles.text}>{`${film.Title}`}</Text>
                          <Text
                            style={styles.text}
                          >{`Year: ${film.Year}`}</Text>
                          <Text
                            style={styles.text}
                          >{`Type: ${film.Type}`}</Text>
                        </View>
                        {id && (
                          <Button
                            title="Delete"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                            onPress={() => this.deleteFilm(id)}
                          />
                        )}
                      </View>
                    </TouchableHighlight>
                  );
                })}
              </>
            );
          }
        })()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainSize: {
    width: "100%",
  },
  margTop: {
    marginTop: 40,
    padding: 20,
  },
  film: {
    // width:350,
    height: "100%",
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  imageSize: {
    width: 100,
    height: 100,
    margin: 20,
    borderWidth: 1,
    borderColor: "gray",
  },
  infoBlock: {
    marginTop: 20,
    marginBottom: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
  text: {
    flex: 1,
    flexWrap: "wrap",
  },
});

export default Films;
