import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default class TimeYAD extends React.Component {
  constructor(props) {
    super(props),
      (this.state = {
        ss: "0",
        mm: "0",
        hh: "0",
      });
    this.timeNow = new Date();
    this.randomTime = new Date(0, 0, 0, 5, 27, 41);
    this.sumDate = this.sumDate.bind(this);
  }

  setState(data) {
    this.state = {
      ...this.state,
      ...data,
    };
  }

  convertTime(date) {
    this.setDate(date);
    return (
      <>{`${this.state.hh > 12 ? this.state.hh - 12 : this.state.hh}:${
        this.state.mm
      }:${this.state.ss}${this.state.hh > 12 ? " AM" : " PM"}`}</>
    );
  }

  checkValidationTime(date) {
    const ruleSS = date.ss > 0 && date.ss < 59;
    const ruleMM = date.mm > 0 && date.mm < 59;
    const ruleHH = date.hh > 0 && date.hh < 23;
    return ruleSS && ruleMM && ruleHH;
  }
  setNullDate() {
    this.setState({
      ss: "0",
      mm: "0",
      hh: "0",
    });
    return <>{this.getTime()}</>;
  }
  setCustomDate(date) {
    if (this.checkValidationTime(date)) {
      this.setState({
        ss: date.ss,
        mm: date.mm,
        hh: date.hh,
      });
      return <>{this.getTime()}</>;
    } else {
      return <>Введіть правильні значення часу!</>;
    }
  }
  setDate(date) {
    const time = date.toLocaleString().split(",")[1].split(":");
    if (date instanceof Date) {
      this.setState({
        ss: time[2],
        mm: time[1],
        hh: time[0],
      });
      return <>{this.getTime()}</>;
    } else {
      return <>Дані передані в метод setDate мають тип відмінний від Date!</>;
    }
  }
  getTime() {
    const time = `${this.state.hh}:${this.state.mm}:${this.state.ss}`;
    return time;
  }
  sumDate(firstDate, secondDate) {
    if (firstDate instanceof Date && secondDate instanceof Date) {
      const time = firstDate.toLocaleString().split(",")[1].split(":");
      const result = new Date(
        secondDate.getTime() +
          time[0] * 3600000 +
          time[1] * 60000 +
          time[2] * 1000
      )
        .toLocaleString()
        .split(",")[1]
        .split(":");
      this.setState({
        ss: result[2],
        mm: result[1],
        hh: result[0],
      });
      return <>{this.getTime()}</>;
    } else if (this.checkValidationTime(secondDate)) {
      let newTime = new Date(
        new Date(
          0,
          0,
          0,
          Number(this.state.hh),
          Number(this.state.mm),
          Number(this.state.ss)
        ).getTime() +
          secondDate.hh * 3600000 +
          secondDate.mm * 60000 +
          secondDate.ss * 1000
      );
      newTime = newTime.toLocaleString().split(",")[1].split(":");
      this.setState({
        ss: newTime[2],
        mm: newTime[1],
        hh: newTime[0],
      });
      return this.getTime();
    } else {
      return "Введіть правильні значення часу!";
    }
  }
  diffDate(firstDate, secondDate) {
    if (firstDate instanceof Date && secondDate instanceof Date) {
      const time = secondDate.toLocaleString().split(",")[1].split(":");
      const result = new Date(
        firstDate.getTime() -
          (time[0] * 3600000 + time[1] * 60000 + time[2] * 1000)
      )
        .toLocaleString()
        .split(",")[1]
        .split(":");
      this.setState({
        ss: result[2],
        mm: result[1],
        hh: result[0],
      });
      return <>{this.getTime()}</>;
    } else if (typeof secondDate === Date) {
      let newTime = new Date(
        new Date(
          0,
          0,
          0,
          Number(this.state.hh),
          Number(this.state.mm),
          Number(this.state.ss)
        ).getTime() -
          (date.hh * 3600000 + date.mm * 60000 + date.ss * 1000)
      );
      newTime = newTime.toLocaleString().split(",");
      return <>{this.getTime()}</>;
    } else {
      return "Введіть правильні значення часу!";
    }
  }

  render() {
    const timeNow = this.timeNow.toLocaleString().split(",")[1];
    const randomTime = this.randomTime.toLocaleString().split(",")[1];
    return (
      <View style={styles.container}>
        <Text style={styles.textInfo}>
          Сума {timeNow} і {randomTime}:{" "}
          {this.sumDate(this.randomTime, this.timeNow)}
        </Text>
        <Text style={styles.textInfo}>
          Різниця {timeNow} і {randomTime.toLocaleString().split(",")[1]}:{" "}
          {this.diffDate(this.timeNow, this.randomTime)}
        </Text>
        <Text style={styles.textInfo}>
          {this.diffDate("timeNow", "timeNow")}
        </Text>
        <Text style={styles.textInfo}>
          Переведення часу {timeNow}: {this.convertTime(this.timeNow)}
        </Text>
        <Text style={styles.textInfo}>
          Встановимо нове значення: {this.setCustomDate({ss:32,mm:41,hh:4})}
        </Text>
        <Text style={styles.textInfo}>
          Сума встановленого занченя і {timeNow}: {this.sumDate('',{ss:timeNow.split(':')[2],mm:timeNow.split(':')[1],hh:timeNow.split(':')[0]})}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "skyblue",
    flex: 0.92,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  navBar: {
    width: "100%",
    flex: 0.08,
  },
  textInfo: {
    textAlign: "center",
  },
});
